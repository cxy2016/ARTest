//
//  Plane.m
//  ARTest1
//
//  Created by CTZ on 2017/8/30.
//  Copyright © 2017年 325. All rights reserved.
//

#import "Plane.h"
@interface Plane()
@property (nonatomic,strong) ARPlaneAnchor *anchor;
@property (nonatomic,strong) SCNBox *planeGeometry;
@end
@implementation Plane
-(instancetype)initWithAnchor:(ARPlaneAnchor *)anchor
{
    self=[super init];
    self.anchor=anchor;
    float width=anchor.extent.x;
    float length=anchor.extent.z;
    float height=0.01;
    
    self.planeGeometry=[SCNBox boxWithWidth:width height:height length:length chamferRadius:0];
    SCNMaterial *transparentMaterial = [SCNMaterial new];
    transparentMaterial.diffuse.contents = [UIColor orangeColor];//[UIColor colorWithWhite:1.0 alpha:1.0];
    
    self.planeGeometry.materials=@[transparentMaterial,
                                   transparentMaterial,
                                   transparentMaterial,
                                   transparentMaterial,
                                   transparentMaterial,
                                   transparentMaterial];
    
    SCNNode *planeNode=[SCNNode nodeWithGeometry:self.planeGeometry];
    planeNode.position=SCNVector3Make(0, -height/2, 0);
    
    [self addChildNode:planeNode];
    
    return self;
}
- (void)update:(ARPlaneAnchor *)anchor {
    // As the user moves around the extend and location of the plane
    // may be updated. We need to update our 3D geometry to match the
    // new parameters of the plane.
    self.planeGeometry.width = anchor.extent.x;
    self.planeGeometry.length = anchor.extent.z;
    
    // When the plane is first created it's center is 0,0,0 and the nodes
    // transform contains the translation parameters. As the plane is updated
    // the planes translation remains the same but it's center is updated so
    // we need to update the 3D geometry position
    self.position = SCNVector3Make(anchor.center.x, 0, anchor.center.z);
    
//    SCNNode *node = [self.childNodes firstObject];
//    node.physicsBody = [SCNPhysicsBody
//                        bodyWithType:SCNPhysicsBodyTypeKinematic
//                        shape: [SCNPhysicsShape shapeWithGeometry:self.planeGeometry options:nil]];
//    [self setTextureScale];
}

- (void)setTextureScale {
    CGFloat width = self.planeGeometry.width;
    CGFloat height = self.planeGeometry.length;
    
    // As the width/height of the plane updates, we want our tron grid material to
    // cover the entire plane, repeating the texture over and over. Also if the
    // grid is less than 1 unit, we don't want to squash the texture to fit, so
    // scaling updates the texture co-ordinates to crop the texture in that case
    SCNMaterial *material = self.planeGeometry.materials[4];
    //NSLog(@"width: %f, height: %f", width, height);
    float scaleFactor = 1;
    SCNMatrix4 m = SCNMatrix4MakeScale(width * scaleFactor, height * scaleFactor, 1);
    material.diffuse.contentsTransform = m;
    material.roughness.contentsTransform = m;
    material.metalness.contentsTransform = m;
    material.normal.contentsTransform = m;
}
@end
