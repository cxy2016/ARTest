//
//  ViewController.m
//  ARTest1
//
//  Created by CTZ on 2017/8/28.
//  Copyright © 2017年 325. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <ARSCNViewDelegate,ARSessionDelegate>
@property (nonatomic,strong) NSMutableDictionary<id,Plane *> *planes;
@property (nonatomic,strong) Plane *plane;
@property (nonatomic, strong) IBOutlet ARSCNView *sceneView;
@property(strong,nonatomic)SCNNode *sunNode,*earthNode,*moonNode,*earthGroupNode,*sunHaloNode,*circleNode1,*circleNode2;
@property(nonatomic,strong) ARWorldTrackingSessionConfiguration *configuration;
@end

    
@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Set the view's delegate
    self.planes=[NSMutableDictionary new];
    [self initScene];
//    scene.rootNode
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Create a session configuration
    self.configuration = [ARWorldTrackingSessionConfiguration new];
    self.configuration.lightEstimationEnabled=YES;
    self.configuration.planeDetection=ARPlaneDetectionHorizontal;
    // Run the view's session
    [self.sceneView.session runWithConfiguration:self.configuration];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Pause the view's session
    [self.sceneView.session pause];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - ARSCNViewDelegate


// Override to create and configure nodes for anchors added to the view's session.
//- (SCNNode *)renderer:(id<SCNSceneRenderer>)renderer nodeForAnchor:(ARAnchor *)anchor {
//- (SCNNode *)renderer:(id<SCNSceneRenderer>)renderer nodeForAnchor:(ARAnchor *)anchor{
//
////     Add geometry to the node...
//    if([anchor isKindOfClass:[ARPlaneAnchor class]])
//    {
//        Plane *plane=[self.planes objectForKey:anchor.identifier];
//        if(!plane)
//        {
//            plane=[[Plane alloc] initWithAnchor:(ARPlaneAnchor *)anchor];
//            [self.planes setObject:plane forKey:anchor.identifier];
//        }
//        return plane;
//    }else
//    {
//        SCNNode *node = [SCNNode new];
//        return node;
//    }
//
//}

-(void)renderer:(id<SCNSceneRenderer>)renderer didAddNode:(SCNNode *)node forAnchor:(ARAnchor *)anchor
{
    if([anchor isKindOfClass:[ARPlaneAnchor class]])
    {
        NSLog(@"添加地面");
//        if(self.plane)
//        {
//            [self.plane update:(ARPlaneAnchor *)anchor];
//        }else
//        {
//            Plane *plane=[[Plane alloc] initWithAnchor:(ARPlaneAnchor *)anchor];
//            [self.planes setObject:plane forKey:anchor.identifier];
//        }
        Plane *plane=[self.planes objectForKey:anchor.identifier];
        if(!plane)
        {
            plane=[[Plane alloc] initWithAnchor:(ARPlaneAnchor *)anchor];
            [self.planes setObject:plane forKey:anchor.identifier];
        }

        [node addChildNode:plane];
    }
}
-(void)renderer:(id<SCNSceneRenderer>)renderer didUpdateNode:(SCNNode *)node forAnchor:(ARAnchor *)anchor
{
    Plane *plane=[self.planes objectForKey:anchor.identifier];
    [plane update:(ARPlaneAnchor *)anchor];
}
-(void)renderer:(id<SCNSceneRenderer>)renderer didRemoveNode:(SCNNode *)node forAnchor:(ARAnchor *)anchor
{
    [self.planes removeObjectForKey:anchor.identifier];
}
- (void)session:(ARSession *)session didFailWithError:(NSError *)error {
    // Present an error message to the user
    
}

- (void)sessionWasInterrupted:(ARSession *)session {
    // Inform the user that the session has been interrupted, for example, by presenting an overlay
    NSLog(@"干扰中..........................");
}

- (void)sessionInterruptionEnded:(ARSession *)session {
    // Reset tracking and/or remove existing anchors if consistent tracking is required
    [self.sceneView.session runWithConfiguration:self.configuration];
    NSLog(@"干扰结束..........................");
}
#pragma mark - ARSessionDelegate
-(void)session:(ARSession *)session didUpdateFrame:(ARFrame *)frame
{
//    NSLog(@"arFrame>>>%@",[frame description]);
}
#pragma  mark -太阳系

-(void)initScene
{
    
    self.sceneView.debugOptions= ARSCNDebugOptionShowFeaturePoints;
    
    self.sceneView.delegate = self;
    self.sceneView.session.delegate=self;
    
    // Show statistics such as fps and timing information
    self.sceneView.showsStatistics = YES;
//    self.sceneView.allowsCameraControl=YES;
    
    // Create a new scene
    SCNScene *scene = [SCNScene new];// sceneNamed:@"art.scnassets/dae/shayu.DAE"];// sceneNamed:@"art.scnassets/ship.scn"];
    
    // Set the scene to the view
    self.sceneView.scene = scene;
    self.sceneView.antialiasingMode=SCNAntialiasingModeMultisampling4X;
    
        SCNNode *cameraNode=[SCNNode node];
        cameraNode.camera=[SCNCamera camera];
        [scene.rootNode addChildNode:cameraNode];
    
        cameraNode.position=SCNVector3Make(0,3,18);
        cameraNode.camera.zFar=100;
        cameraNode.rotation=SCNVector4Make(1, 0, 0, -M_PI_4/4);
    
    //隐藏默认模型
    SCNNode *ship=[scene.rootNode childNodeWithName:@"ship" recursively:YES];
    [ship setHidden:YES];
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPlane:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.sceneView addGestureRecognizer:tapGesture];
    
    UIPinchGestureRecognizer *pinGesture=[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinNodeHandle:)];
    [self.sceneView addGestureRecognizer:pinGesture];
//    [self initNode];
}
-(void)initNode{
    if(_sunNode)
    {
        return;
    }
    _sunNode = [SCNNode new];
    _earthNode = [SCNNode new];
    _moonNode = [SCNNode new];
    _earthGroupNode = [SCNNode new];
    
    _sunNode.geometry = [SCNSphere sphereWithRadius:2.5];
    [self.sceneView.scene.rootNode addChildNode:_sunNode];
    _sunNode.position=SCNVector3Make(0, -10, -40);
    
    _earthNode.geometry = [SCNSphere sphereWithRadius:1.0];
    [_earthGroupNode addChildNode:_earthNode];
    _earthGroupNode.position = SCNVector3Make(10, 0, 0);
    
    _moonNode.geometry = [SCNSphere sphereWithRadius:0.5];
    _moonNode.position = SCNVector3Make(3, 1, 0);
    
    _circleNode1=[self createCircleNode:[UIColor greenColor]];
    _circleNode2=[self createCircleNode:[UIColor redColor]];
    
    [_sunNode addChildNode:_circleNode1];
    [_sunNode addChildNode:_circleNode2];
    
    
    
    // Add materials to the planets
    _earthNode.geometry.firstMaterial.diffuse.contents = @"art.scnassets/earth/earth-diffuse-mini.jpg";
    _earthNode.geometry.firstMaterial.emission.contents = @"art.scnassets/earth/earth-emissive-mini.jpg";
    _earthNode.geometry.firstMaterial.specular.contents = @"art.scnassets/earth/earth-specular-mini.jpg";
    _moonNode.geometry.firstMaterial.diffuse.contents = @"art.scnassets/earth/moon.jpg";
    _sunNode.geometry.firstMaterial.multiply.contents = @"art.scnassets/earth/sun.jpg";
    _sunNode.geometry.firstMaterial.diffuse.contents = @"art.scnassets/earth/sun.jpg";
    _sunNode.geometry.firstMaterial.multiply.intensity = 0.5;
    _sunNode.geometry.firstMaterial.lightingModelName = SCNLightingModelConstant;
    
    _sunNode.geometry.firstMaterial.multiply.wrapS =
    _sunNode.geometry.firstMaterial.diffuse.wrapS  =
    _sunNode.geometry.firstMaterial.multiply.wrapT =
    _sunNode.geometry.firstMaterial.diffuse.wrapT  = SCNWrapModeRepeat;
    
    _earthNode.geometry.firstMaterial.locksAmbientWithDiffuse =
    _moonNode.geometry.firstMaterial.locksAmbientWithDiffuse  =
    _sunNode.geometry.firstMaterial.locksAmbientWithDiffuse   = YES;
    
    _earthNode.geometry.firstMaterial.shininess = 0.1;
    _earthNode.geometry.firstMaterial.specular.intensity = 0.5;
    _moonNode.geometry.firstMaterial.specular.contents = [UIColor grayColor];

    
    
    [self roationNode];
    [self addOtherNode];
    [self addLight];
    
}
-(SCNNode *)createCircleNode:(UIColor *)color
{
    SCNNode *node=[SCNNode new];
    SCNSphere *sphere=[SCNSphere sphereWithRadius:0.1];
    sphere.firstMaterial.diffuse.contents=color;//[UIColor greenColor];
    node.geometry=sphere;
    
    return node;
}
-(void)roationNode{
    
    [_earthNode runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:0 y:2 z:0 duration:1]]];   //地球自转
    
    // Rotate the moon
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"rotation"];        //月球自转
    animation.duration = 1.5 ;
    animation.toValue = [NSValue valueWithSCNVector4:SCNVector4Make(0, 1, 0, M_PI * 2)];
    animation.repeatCount = FLT_MAX;
    //    [_moonNode addAnimation:animation forKey:@"moon rotation"];
    
    [_moonNode runAction:[SCNAction repeatActionForever:[SCNAction rotateByX:0 y:4 z:0 duration:1.0]]];
    
    
    // Moon-rotation (center of rotation of the Moon around the Earth)
    SCNNode *moonRotationNode = [SCNNode node];
    
    [moonRotationNode addChildNode:_moonNode];
    
    // Rotate the moon around the Earth
    CABasicAnimation *moonRotationAnimation = [CABasicAnimation animationWithKeyPath:@"rotation"];
    moonRotationAnimation.duration = 5.0;
    moonRotationAnimation.toValue = [NSValue valueWithSCNVector4:SCNVector4Make(0, 1, 0, M_PI * 2)];
    moonRotationAnimation.repeatCount = FLT_MAX;
    [moonRotationNode addAnimation:animation forKey:@"moon rotation around earth"];
    
    
    [_earthGroupNode addChildNode:moonRotationNode];
    
    
    if(YES){    //  normal Roation
        
        // Earth-rotation (center of rotation of the Earth around the Sun)
        SCNNode *earthRotationNode = [SCNNode node];
        [_sunNode addChildNode:earthRotationNode];
        
        // Earth-group (will contain the Earth, and the Moon)
        [earthRotationNode addChildNode:_earthGroupNode];
        
        // Rotate the Earth around the Sun
        animation = [CABasicAnimation animationWithKeyPath:@"rotation"];
        animation.duration = 10.0;
        animation.toValue = [NSValue valueWithSCNVector4:SCNVector4Make(0, 1, 0, M_PI * 2)];
        animation.repeatCount = FLT_MAX;
        [earthRotationNode addAnimation:animation forKey:@"earth rotation around sun"];
        
    }
    else{   // math roation
        
        [_sunNode addChildNode:_earthGroupNode];
        [self mathRoation];
    }
    
    
    [self addAnimationToSun];
}
-(void)addAnimationToSun{
    
    // Achieve a lava effect by animating textures
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"contentsTransform"];
    animation.duration = 10.0;
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DConcat(CATransform3DMakeTranslation(0, 0, 0), CATransform3DMakeScale(3, 3, 3))];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DConcat(CATransform3DMakeTranslation(1, 0, 0), CATransform3DMakeScale(3, 3, 3))];
    animation.repeatCount = FLT_MAX;
    [_sunNode.geometry.firstMaterial.diffuse addAnimation:animation forKey:@"sun-texture"];
    
    animation = [CABasicAnimation animationWithKeyPath:@"contentsTransform"];
    animation.duration = 30.0;
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DConcat(CATransform3DMakeTranslation(0, 0, 0), CATransform3DMakeScale(5, 5, 5))];
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DConcat(CATransform3DMakeTranslation(1, 0, 0), CATransform3DMakeScale(5, 5, 5))];
    animation.repeatCount = FLT_MAX;
    [_sunNode.geometry.firstMaterial.multiply addAnimation:animation forKey:@"sun-texture2"];
    
}
-(void)mathRoation{
    
    // 相关数学知识点： 任意点a(x,y)，绕一个坐标点b(rx0,ry0)逆时针旋转a角度后的新的坐标设为c(x0, y0)，有公式：
    
    //    x0= (x - rx0)*cos(a) - (y - ry0)*sin(a) + rx0 ;
    //
    //    y0= (x - rx0)*sin(a) + (y - ry0)*cos(a) + ry0 ;
    
    // custom Action
    
    float totalDuration = 10.0f;        //10s 围绕地球转一圈
    float duration = totalDuration/360;  //每隔duration秒去执行一次
    
    
    SCNAction *customAction = [SCNAction customActionWithDuration:duration actionBlock:^(SCNNode * _Nonnull node, CGFloat elapsedTime){
        
        
        if(elapsedTime==duration){
            
            
            SCNVector3 position = node.position;
            
            float rx0 = 0;    //原点为0
            float ry0 = 0;
            
            float angle = 1.0f/180*M_PI;
            
            float x =  (position.x - rx0)*cos(angle) - (position.z - ry0)*sin(angle) + rx0 ;
            
            float z = (position.x - rx0)*sin(angle) + (position.z - ry0)*cos(angle) + ry0 ;
            
            node.position = SCNVector3Make(x, node.position.y, z);
            
        }
        
    }];
    
    SCNAction *repeatAction = [SCNAction repeatActionForever:customAction];
    
    [_earthGroupNode runAction:repeatAction];
}
-(void)addLight{
    
    // We will turn off all the lights in the scene and add a new light
    // to give the impression that the Sun lights the scene
    SCNNode *lightNode = [SCNNode node];
    lightNode.light = [SCNLight light];
    lightNode.light.color = [UIColor blackColor]; // initially switched off
    lightNode.light.type = SCNLightTypeOmni;
    [_sunNode addChildNode:lightNode];
    
    // Configure attenuation distances because we don't want to light the floor
    lightNode.light.attenuationEndDistance = 20.0;
    lightNode.light.attenuationStartDistance = 19.5;
    
    // Animation
    [SCNTransaction begin];
    [SCNTransaction setAnimationDuration:1];
    {

        lightNode.light.color = [UIColor whiteColor]; // switch on
        //[presentationViewController updateLightingWithIntensities:@[@0.0]]; //switch off all the other lights
        _sunHaloNode.opacity = 0.5; // make the halo stronger
    }
    [SCNTransaction commit];
}
-(void)addOtherNode{
    
    
    SCNNode *cloudsNode = [SCNNode node];
    cloudsNode.geometry = [SCNSphere sphereWithRadius:1.15];
    
    [_earthNode addChildNode:cloudsNode];
    
    cloudsNode.opacity = 0.5;
    // This effect can also be achieved with an image with some transparency set as the contents of the 'diffuse' property
    cloudsNode.geometry.firstMaterial.transparent.contents = @"art.scnassets/earth/cloudsTransparency.png";
    cloudsNode.geometry.firstMaterial.transparencyMode = SCNTransparencyModeRGBZero;
    
    // Add a halo to the Sun (a simple textured plane that does not write to depth)
    _sunHaloNode = [SCNNode node];
    _sunHaloNode.geometry = [SCNPlane planeWithWidth:25 height:25];
    _sunHaloNode.rotation = SCNVector4Make(1, 0, 0, 0 * M_PI / 180.0);
    _sunHaloNode.geometry.firstMaterial.diffuse.contents = @"art.scnassets/earth/sun-halo.png";
    _sunHaloNode.geometry.firstMaterial.lightingModelName = SCNLightingModelConstant; // no lighting
    _sunHaloNode.geometry.firstMaterial.writesToDepthBuffer = NO; // do not write to depth
    _sunHaloNode.opacity = 0.2;
    [_sunNode addChildNode:_sunHaloNode];
    
    
    // Add a textured plane to represent Earth's orbit
    SCNNode *earthOrbit = [SCNNode node];
    earthOrbit.opacity = 0.4;
    earthOrbit.geometry = [SCNPlane planeWithWidth:21 height:21];
    earthOrbit.geometry.firstMaterial.diffuse.contents = @"art.scnassets/earth/orbit.png";
    earthOrbit.geometry.firstMaterial.diffuse.mipFilter = SCNFilterModeLinear;
    earthOrbit.rotation = SCNVector4Make(1, 0, 0,-M_PI_2);
    earthOrbit.geometry.firstMaterial.lightingModelName = SCNLightingModelConstant; // no lighting
    [_sunNode addChildNode:earthOrbit];
}

- (void) handleTap:(UIGestureRecognizer*)gestureRecognize
{
    
    // check what nodes are tapped
    CGPoint p = [gestureRecognize locationInView:self.sceneView];
    NSArray *hitResults = [self.sceneView hitTest:p options:nil];
    
    // check that we clicked on at least one object
    if([hitResults count] > 0){
        // retrieved the first clicked object
        SCNHitTestResult *result = [hitResults objectAtIndex:0];
        
        // get its material
        SCNMaterial *material = result.node.geometry.firstMaterial;
        
        // highlight it
        [SCNTransaction begin];
        [SCNTransaction setAnimationDuration:0.5];
        
        // on completion - unhighlight
        [SCNTransaction setCompletionBlock:^{
            [SCNTransaction begin];
            [SCNTransaction setAnimationDuration:0.5];
            
            material.emission.contents = [UIColor blackColor];
            
            [SCNTransaction commit];
        }];
        
        material.emission.contents = [UIColor redColor];
        
        [SCNTransaction commit];
    }
}
#pragma mark - 交互(自动) Start
-(void)tapPlane:(UIGestureRecognizer *)gesture
{
    CGPoint tapPoint = [gesture locationInView:self.sceneView];
    NSArray<ARHitTestResult *> *result = [self.sceneView hitTest:tapPoint types:ARHitTestResultTypeExistingPlaneUsingExtent];
    
    // If the intersection ray passes through any plane geometry they will be returned, with the planes
    // ordered by distance from the camera
    if (result.count == 0) {
        return;
    }
    
    // If there are multiple hits, just pick the closest plane
    ARHitTestResult * hitResult = [result firstObject];
    SCNVector3 position = SCNVector3Make(
                                         hitResult.worldTransform.columns[3].x,
                                         hitResult.worldTransform.columns[3].y,
                                         hitResult.worldTransform.columns[3].z
                                         );
    if(NO){
        [self initNode];
        [self.sunNode runAction:[SCNAction scaleTo:0.03 duration:0.5]];
        SCNVector3 minV3;
        SCNVector3 maxV3;
        [self.sunNode.geometry getBoundingBoxMin:&minV3 max:&maxV3];
        self.circleNode1.position=minV3;
        self.circleNode2.position=maxV3;
        CGFloat height=maxV3.y-minV3.y;
        position.y+=height/2*0.03;
        self.sunNode.position=position;
        [self testSizeOfNode:self.sunNode];
    }else{
        [self loadDaeWithPosition:position];
    }
}

-(void)pinNodeHandle:(UIPinchGestureRecognizer *)gesture
{
    if(gesture.state==UIGestureRecognizerStateCancelled)
    {
        return;
    }
    CGPoint tapPoint = [gesture locationInView:self.sceneView];
    NSArray<SCNHitTestResult *> *result = [self.sceneView hitTest:tapPoint options:@{SCNHitTestBoundingBoxOnlyKey: @YES, SCNHitTestFirstFoundOnlyKey: @YES}];//[self.sceneView hitTest:tapPoint types:ARHitTestResultTypeExistingPlaneUsingExtent];
    
    // If the intersection ray passes through any plane geometry they will be returned, with the planes
    // ordered by distance from the camera
    if (result.count == 0) {
        return;
    }
    
    // If there are multiple hits, just pick the closest plane
    
    SCNHitTestResult * hitResult = [result firstObject];
    if(hitResult.node!=self.sunNode)
    {
        SCNNode *selecteNode=hitResult.node;
        //    SCNVector3 position = SCNVector3Make(
        //                                         hitResult.worldTransform.columns[3].x,
        //                                         hitResult.worldTransform.columns[3].y+0.1,
        //                                         hitResult.worldTransform.columns[3].z
        //                                         );
        [selecteNode runAction:[SCNAction scaleTo:gesture.scale*0.1 duration:0.3]];
        //    [self.sunNode runAction:[SCNAction scaleTo:0.03 duration:0.5]];
        //    self.sunNode.position=position;
    }
}
-(void)testSizeOfNode:(SCNNode *)node
{
    SCNVector3 minV3;
    SCNVector3 maxV3;
    if(node.geometry){
        [node.geometry getBoundingBoxMin:&minV3 max:&maxV3];
    }else{
        [node getBoundingBoxMin:&minV3 max:&maxV3];
    }
    NSLog(@"尺寸>>>%@---%@",NSStringFromGLKVector3(SCNVector3ToGLKVector3(minV3)),
          NSStringFromGLKVector3(SCNVector3ToGLKVector3(maxV3)));
}
#pragma mark 交互(自动) End
-(void)loadDaeWithPosition:(SCNVector3)position
{
//    SCNSceneSource *source=[[SCNSceneSource alloc] init];
    SCNScene *deaScene=[SCNScene sceneNamed:@"art.scnassets/dae/shayu.DAE"];
    SCNNode *shayu=deaScene.rootNode;//[deaScene.rootNode childNodeWithName:@"shark" recursively:YES];
    [self.sceneView.scene.rootNode addChildNode:shayu];
    shayu.position=position;
    [shayu runAction:[SCNAction scaleTo:0.1 duration:0.5]];
    [self testSizeOfNode:shayu];
//    deaScene.rootNode
}
@end
