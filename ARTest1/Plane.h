//
//  Plane.h
//  ARTest1
//
//  Created by CTZ on 2017/8/30.
//  Copyright © 2017年 325. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import <ARKit/ARKit.h>
/**
 地板砖
 */
@interface Plane : SCNNode
-(instancetype)initWithAnchor:(ARPlaneAnchor *)anchor;
- (void)update:(ARPlaneAnchor *)anchor;
- (void)setTextureScale;
@end
